FROM openjdk:8-jre-alpine
RUN ["mkdir" , "-p" , "/usr/local/mensajes/data"]
WORKDIR /usr/local/mensajes/data
COPY ./target/api_rest_mensajes-0.0.1-SNAPSHOT.jar /usr/local/api_rest_mensajes/app.jar
EXPOSE 8080
VOLUME [ "/usr/local/ape_rest_mensajes/data" ]
CMD ["java" , "-jar" , "/usr/local/api_rest_mensajes/app.jar"]