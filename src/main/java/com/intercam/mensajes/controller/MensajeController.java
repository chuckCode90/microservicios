package com.intercam.mensajes.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.intercam.mensajes.vo.MensajeVO;


@RestController
@RequestMapping(value = "api/mensajes")
public class MensajeController {
/*codigo para recuperar/guardar en un path local*/	
	private static final String PATH_BASE = "/usr/local/mensajes/data";

    @GetMapping
    public List<MensajeVO> findAllContenido() throws IOException {
        List<MensajeVO> list = new ArrayList<>();
        final File folder = new File(PATH_BASE);
        for (final File fileEntry : folder.listFiles()) {
        	MensajeVO agregarMsj = new MensajeVO(Integer.parseInt(fileEntry.getName()), new String(Files.readAllBytes(Paths.get(fileEntry.toString()))));
            list.add(agregarMsj);
        }
        return list;
    }

    @PostMapping
    public MensajeVO createContenido(@RequestBody MensajeVO mensajeVO) throws IOException {
        Files.write(Paths.get(PATH_BASE + "/" + mensajeVO.getId()), mensajeVO.getMensaje().getBytes());
        return mensajeVO;
    }

//	
//	@Autowired
//	private MensajeRepository repository;
//	
//	@GetMapping(value = "/")
//	public ResponseEntity<List<MensajeVO>> findAll(){
//		List<MensajeVO> list = null;
//		try {
//			List<Mensajes> entities = repository.findAll();
//			if (entities == null || entities.isEmpty()) {
//				return new ResponseEntity(list, HttpStatus.NO_CONTENT);
//			}
//			list = new ArrayList<>();
//			for (Mensajes c : entities) {
//				list.add(new MensajeVO(c.getId(), c.getMensaje()));
//			}
//			return new ResponseEntity<List<MensajeVO>>(list, HttpStatus.OK);
//		} catch (Exception e) {
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(list);
//		}
//	}
//	
//	@PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, 
//			produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
//	public ResponseEntity<MensajeVO> save(@RequestBody MensajeVO mensaje) {
//		MensajeVO response = null;
//		try {
//			Mensajes entity = new Mensajes(mensaje.getId(), mensaje.getMensaje());
//			Mensajes re = repository.save(entity);
//			response = new MensajeVO(re.getId(), re.getMensaje());
//			return new ResponseEntity<MensajeVO>(response, HttpStatus.CREATED);
//		} catch (Exception e) {
//			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
//		}
//	}
		
}
