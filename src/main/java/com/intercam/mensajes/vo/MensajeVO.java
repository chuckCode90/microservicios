package com.intercam.mensajes.vo;


import java.io.Serializable;


public class MensajeVO implements Serializable {
	
	Integer id;
	String mensaje;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public MensajeVO(Integer id, String mensaje) {
		super();
		this.id = id;
		this.mensaje = mensaje;
	}
	
}
